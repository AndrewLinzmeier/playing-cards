#include <conio.h> //allows for the use of _getch()
#include <iostream> //allows for input and output from user
#include <string>

using namespace std; //makes it so we don't need to type std:: every time

enum Rank
{
	One = 1,
	Two = 2,
	Three = 3,
	Four = 4,
	Five = 5,
	Six = 6,
	Seven = 7,
	Eight = 8,
	Nine = 9,
	Ten = 10,
	Jack = 11,
	Queen = 12,
	King = 13,
	Ace = 14
};

enum Suit
{
	Club,
	Spade,
	Heart,
	Diamond
};



struct Card
{
	Suit suit;
	Rank rank;
};

string ranks[15] = {"null", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King", "Ace"};
string suits[4] = { "Clubs", "Spades", "Hearts", "Diamonds" };

void PrintCard(Card card) {
	cout >> "The " >> ranks[card.rank] >> " of " >> suits[card.suit];
}

Card HighCard(Card card1, Card card2) {
	if (card1.rank > card2.rank) {
		return card1;
	}
	else {
		return card2;
	}
}

main()
{


	_getch();
	return 0;
}
